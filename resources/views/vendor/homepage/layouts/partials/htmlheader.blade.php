  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="keywords" content="Bootstrap, Landing page, Template, Registration, Landing">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="author" content="Grayrids">
    <title>@yield('htmlheader_title', 'Your title here')</title>
    <link href="{{asset('img/elit-logo-2.png')}}" rel="shortcut icon">
     <!-- ============================================ -->
    <!-- <link rel="stylesheet" href="{{asset('css/data-table/dataTables.bootstrap4.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/data-table/bootstrap.css')}}"> -->
    <link rel="stylesheet" href="{{asset('css/data-table/jquery.dataTables.min.css')}}">
    
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('fontawesome/css/all.css')}}">
    <link rel="stylesheet" href="{{asset('css/line-icons.css')}}">
    <link rel="stylesheet" href="{{asset('css/owl.carousel.css')}}">
    <link rel="stylesheet" href="{{asset('css/owl.theme.css')}}">
    <link rel="stylesheet" href="{{asset('css/nivo-lightbox.css')}}">
    <link rel="stylesheet" href="{{asset('css/magnific-popup.css')}}">
    <link rel="stylesheet" href="{{asset('css/animate.css')}}">

    <!-- <link rel="stylesheet" href="{{asset('css/color-switcher.css')}}"> -->
    <link rel="stylesheet" href="{{asset('css/menu_sideslide.css')}}">
    <link rel="stylesheet" href="{{asset('css/main.css')}}">    
    <link rel="stylesheet" href="{{asset('css/responsive.css')}}">
    <link rel="stylesheet" href="{{asset('css/bootstrap-toggle.min.css')}}">

   

  </head>