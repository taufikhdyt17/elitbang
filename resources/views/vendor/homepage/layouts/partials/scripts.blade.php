    <script src="{{asset('js/jquery-min.js')}}"></script>
    <script src="{{asset('js/popper.min.js')}}"></script>
    <script src="{{asset('js/bootstrap.min.js')}}"></script>
    <script src="{{asset('js/classie.js')}}"></script>
    <!-- <script src="{{asset('js/color-switcher.js')}}"></script> -->
    <script src="{{asset('js/jquery.mixitup.js')}}"></script>
    <script src="{{asset('js/nivo-lightbox.js')}}"></script>
    <script src="{{asset('js/owl.carousel.js')}}"></script>    
    <script src="{{asset('js/jquery.stellar.min.js')}}"></script>    
    <script src="{{asset('js/jquery.nav.js')}}"></script>    
    <script src="{{asset('js/scrolling-nav.js')}}"></script>    
    <script src="{{asset('js/jquery.easing.min.js')}}"></script>     
    <script src="{{asset('js/wow.js')}}"></script> 
    <script src="{{asset('js/jquery.vide.js')}}"></script>
    <script src="{{asset('js/jquery.counterup.min.js')}}"></script>    
    <script src="{{asset('js/jquery.magnific-popup.min.js')}}"></script>    
    <script src="{{asset('js/waypoints.min.js')}}"></script>    
    <script src="{{asset('js/form-validator.min.js')}}"></script>
    <script src="{{asset('js/contact-form-script.js')}}"></script>   
    <script src="{{asset('js/main.js')}}"></script>
    <script src="{{asset('js/bootstrap-toggle.min.js')}}"></script>
    <script src="{{asset('particlesjs/particles.min.js')}}"></script>
    
    <script src="{{asset('js/data-table/dataTables.bootstrap4.min.js')}}"></script>
    <script src="{{asset('js/data-table/jquery.dataTables.min.js')}}"></script>
   

@yield('custom_scripts')